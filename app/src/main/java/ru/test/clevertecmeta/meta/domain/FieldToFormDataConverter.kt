package ru.test.clevertecmeta.meta.domain

import ru.test.clevertecmeta.meta.data.dto.Field
import ru.test.clevertecmeta.meta.data.dto.FormData

fun Field.toFormData() = FormData(mapOf(this.name to this.value!!))
fun Collection<Field>.toFormData() = FormData(
    mapOf(
        *this.map {
                field -> field.name to field.value!!
        }.toTypedArray()
    )
)