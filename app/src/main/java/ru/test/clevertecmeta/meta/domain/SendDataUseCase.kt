package ru.test.clevertecmeta.meta.domain

import io.reactivex.Observable
import ru.test.clevertecmeta.meta.data.dto.Field
import ru.test.clevertecmeta.meta.data.dto.Result
import ru.test.clevertecmeta.meta.data.repository.FormDataRepository

class SendDataUseCase(
    private val formDataRepository: FormDataRepository
) {
    fun send(fields: List<Field>): Observable<Result> = formDataRepository.send(fields.toFormData())
}