package ru.test.clevertecmeta.meta.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.test.clevertecmeta.meta.data.repository.MetaRepository
import ru.test.clevertecmeta.meta.domain.SendDataUseCase


class MetaViewModelFactory(
    private val metaRepository: MetaRepository,
    private val sendDataUseCase: SendDataUseCase
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MetaViewModel(metaRepository, sendDataUseCase) as T
    }
}