package ru.test.clevertecmeta.meta.data.repository

import io.reactivex.Observable
import ru.test.clevertecmeta.meta.data.dto.FormData
import ru.test.clevertecmeta.meta.data.dto.Result
import ru.test.clevertecmeta.meta.data.remote.FormDataService


class FormDataRepository(
    private val formDataService: FormDataService
) {
    fun send(formData: FormData): Observable<Result> = formDataService.sendData(formData)
}