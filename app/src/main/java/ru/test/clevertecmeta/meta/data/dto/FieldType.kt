package ru.test.clevertecmeta.meta.data.dto

import androidx.room.TypeConverter

enum class FieldType {
    TEXT,
    NUMERIC,
    LIST
}


class FieldTypeConverter {
    @TypeConverter
    fun fromFieldType(fieldType: FieldType): String = fieldType.name

    @TypeConverter
    fun toFieldType(name: String) = FieldType.valueOf(name)
}