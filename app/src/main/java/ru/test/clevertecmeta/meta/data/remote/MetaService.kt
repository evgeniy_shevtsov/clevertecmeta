package ru.test.clevertecmeta.meta.data.remote

import io.reactivex.Observable
import retrofit2.http.POST
import ru.test.clevertecmeta.meta.data.dto.Meta


interface MetaService {

    @POST("meta")
    fun getMeta(): Observable<Meta>
}