package ru.test.clevertecmeta.meta.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.AppCompatSpinner
import com.google.android.material.textfield.TextInputEditText
import ru.test.clevertecmeta.R
import ru.test.clevertecmeta.base.OnItemSelectedListener
import ru.test.clevertecmeta.base.TextChangedListener
import ru.test.clevertecmeta.base.recycler.BaseRecyclerViewAdapter
import ru.test.clevertecmeta.base.recycler.BaseViewHolder
import ru.test.clevertecmeta.meta.data.dto.Field
import ru.test.clevertecmeta.meta.data.dto.FieldType


class FieldsAdapter : BaseRecyclerViewAdapter<Field, FieldsAdapter.BaseFieldViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseFieldViewHolder
            = when (FieldType.values()[viewType]) {
                FieldType.NUMERIC -> NumericFieldViewHolder(parent.inflate(R.layout.numeric_field_list_item))
                FieldType.TEXT -> TextFieldViewHolder(parent.inflate(R.layout.text_field_list_item))
                FieldType.LIST -> ListFieldViewHolder(parent.inflate(R.layout.list_field_list_item))
            }

    override fun getItemViewType(position: Int) = items[position].type.ordinal

    private fun ViewGroup.inflate(@LayoutRes layoutId: Int, attachToRoot: Boolean = false)
            = LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)


    /** View holders */
    open class BaseFieldViewHolder(itemView: View) : BaseViewHolder<Field>(itemView) {
        protected val title: TextView = itemView.findViewById(R.id.field_title)

        override fun bindData(data: Field) {
            super.bindData(data)
            title.text = data.title
        }
    }

    open class TextFieldViewHolder(itemView: View) : BaseFieldViewHolder(itemView) {
        private val valueEditText: TextInputEditText = itemView.findViewById(R.id.value_edit_text)

        override fun bindData(data: Field) {
            super.bindData(data)
            valueEditText.addTextChangedListener(TextChangedListener { text -> data.value = text })
        }
    }

    class NumericFieldViewHolder(itemView: View) : TextFieldViewHolder(itemView)

    class ListFieldViewHolder(itemView: View) : BaseFieldViewHolder(itemView) {
        private val valuesSpinner: AppCompatSpinner = itemView.findViewById(R.id.value_spinner)
        private var values = emptyArray<Map.Entry<String, String>>()

        override fun bindData(data: Field) {
            super.bindData(data)
            data.values?.let { valuesMap -> values = valuesMap.entries.toTypedArray() }

            valuesSpinner.adapter = ArrayAdapter<String>(
                itemView.context,
                android.R.layout.simple_spinner_item,
                values.map { item -> item.value }
            ).apply {
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }

            valuesSpinner.onItemSelectedListener = OnItemSelectedListener { pos -> data.value = values[pos].key }
        }
    }
}



