package ru.test.clevertecmeta.meta.data.database

import androidx.room.*
import ru.test.clevertecmeta.meta.data.dto.Field


@Dao
interface FieldDao {
    @Query("SELECT * FROM fields")
    fun getAll(): List<Field>

    @Query("SELECT * FROM fields WHERE uid IN (:fieldIds)")
    fun get(vararg fieldIds: String): List<Field>

    @Query("SELECT * FROM fields WHERE metaUid IN (:metaUid)")
    fun getByMeta(metaUid: String): List<Field>

    @Insert
    fun insert(vararg fields: Field)

    @Update
    fun update(vararg fields: Field)

    @Delete
    fun delete(vararg fields: Field)

    @Query("DELETE FROM fields WHERE uid IN (:fieldIds)")
    fun delete(vararg fieldIds: String)
}