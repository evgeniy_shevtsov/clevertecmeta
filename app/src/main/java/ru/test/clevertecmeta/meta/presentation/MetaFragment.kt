package ru.test.clevertecmeta.meta.presentation

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.dialog_info.view.*
import kotlinx.android.synthetic.main.dialog_progress.view.*
import kotlinx.android.synthetic.main.fragment_meta.*

import ru.test.clevertecmeta.R
import ru.test.clevertecmeta.base.view.BaseFragment
import ru.test.clevertecmeta.meta.MetaActivity
import javax.inject.Inject


class MetaFragment : BaseFragment() {

    @Inject
    lateinit var metaViewModelFactory: MetaViewModelFactory
    private lateinit var viewModel: MetaViewModel
    private val fieldsAdapter = FieldsAdapter()

    private var loadingDialog: AlertDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_meta, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        observeViewModel()
        initBottomAppBar()
    }

    private fun initRecyclerView() {
        recycler_view.run {
            adapter = fieldsAdapter

            val dividerItemDecoration = DividerItemDecoration(
                context, DividerItemDecoration.VERTICAL)
            ContextCompat.getDrawable(context, R.drawable.list_divider)?.let {
                dividerItemDecoration.setDrawable(it)
            }
            addItemDecoration(dividerItemDecoration)
        }
    }

    private fun initBottomAppBar() = (activity as? MetaActivity)?.run {
        bottomBar().replaceMenu(R.menu.meta_bottom_menu)
        bottomBar().setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_refresh -> {
                    viewModel.refreshMeta()
                    true
                }
                else -> false
            }
        }
        bottomBarButton().setImageResource(R.drawable.outline_send_white_24)
        bottomBarButton().setOnClickListener { viewModel.sendData(fieldsAdapter.items) }
    }

    private fun observeViewModel() {
        viewModel = ViewModelProviders.of(this, metaViewModelFactory).get(MetaViewModel::class.java)

        viewModel.meta.observe(this@MetaFragment, Observer { meta ->
            meta ?: return@Observer

            title.text = meta.title
            fieldsAdapter.updateItems(meta.fields)
            Glide.with(this).load(meta.image).into(bottom_image_view)
        })

        viewModel.isLoading.observe(this@MetaFragment, Observer { loading ->
            loading ?: return@Observer

            if (loading)
                showLoading()
            else
                hideLoading()
        })

        viewModel.result.observe(this@MetaFragment, Observer { result ->
            result?.run{ showMessage(value) }
        })
        viewModel.error.observe(this@MetaFragment, Observer { error ->
            error?.run{ showMessage(getString(error)) }
        })
    }

    private fun showLoading() {
        if (loadingDialog == null) {
            val view = layoutInflater.inflate(R.layout.dialog_progress, null)

            loadingDialog = AlertDialog.Builder(context)
                .setCancelable(false)
                .setView(view)
                .create()

            view.cancel_button.setOnClickListener {
                viewModel.cancelLoading()
                loadingDialog?.dismiss()
            }
        }

        loadingDialog?.run { if (!isShowing) show() }
    }

    private fun hideLoading() = loadingDialog?.dismiss()

    private fun showMessage(text: String) {
        val view = layoutInflater.inflate(R.layout.dialog_info, null)

        val dialog = AlertDialog.Builder(context)
            .setView(view)
            .create()

        view.ok_button.setOnClickListener { dialog.dismiss() }
        view.message.text = text

        dialog.show()
    }
}
