package ru.test.clevertecmeta.meta.data.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class FormData(

    @Expose
    @SerializedName("form")
    var values: Map<String, String>
) : Serializable