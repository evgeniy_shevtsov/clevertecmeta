package ru.test.clevertecmeta.meta.data.dto

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*


@Entity(tableName = "metas")
data class Meta(

    @Expose
    @SerializedName("title")
    @ColumnInfo(name = "title")
    var title: String,

    @Expose
    @SerializedName("image")
    @ColumnInfo(name = "image")
    var image: String,

    @Expose
    @SerializedName("fields")
    @Ignore
    var fields: List<Field>
) : Serializable {

    @ColumnInfo(name = "uid")
    @PrimaryKey
    var uid: String = UUID.randomUUID().toString()

    @ColumnInfo(name = "createTime")
    var createTime = Date().time

    constructor(title: String, image: String, uid: String) : this(title, image, emptyList()) {
        this.uid = uid
    }

    fun generateUid() = run { uid = UUID.randomUUID().toString()}
}

