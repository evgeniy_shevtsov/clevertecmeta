package ru.test.clevertecmeta.meta.domain

import ru.test.clevertecmeta.meta.data.dto.Field
import ru.test.clevertecmeta.meta.data.dto.FieldType

class FieldValidateUtil {
    companion object {
        @JvmStatic
        fun isValid(field: Field): Boolean = when(field.type) {
            FieldType.NUMERIC -> try {
                field.value?.toDouble()
                true
            } catch (ignored: Exception) { false }
            FieldType.TEXT -> field.value?.run { !isEmpty() } ?: false
            FieldType.LIST -> true
        }

        @JvmStatic
        fun isValid(fields: List<Field>): Boolean = fields.none { field -> !isValid(field) }
    }
}