package ru.test.clevertecmeta.meta.data.database

import androidx.room.*
import ru.test.clevertecmeta.meta.data.dto.Meta


@Dao
interface MetaDao {
    @Query("SELECT * FROM metas")
    fun getAll(): List<Meta>

    @Query("SELECT * FROM metas WHERE uid IN (:metaIds)")
    fun get(vararg metaIds: String): List<Meta>

    @Insert
    fun insert(vararg metas: Meta)

    @Update
    fun update(vararg metas: Meta)

    @Delete
    fun delete(vararg metas: Meta)

    @Query("DELETE FROM metas WHERE uid IN (:metaIds)")
    fun delete(vararg metaIds: String)
}