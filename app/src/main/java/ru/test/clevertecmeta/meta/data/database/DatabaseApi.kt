package ru.test.clevertecmeta.meta.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.test.clevertecmeta.meta.data.dto.Field
import ru.test.clevertecmeta.meta.data.dto.FieldTypeConverter
import ru.test.clevertecmeta.meta.data.dto.Meta
import ru.test.clevertecmeta.meta.data.dto.ValuesTypeConverter


@Database(entities = [Meta::class, Field::class], version = 1)
@TypeConverters(ValuesTypeConverter::class, FieldTypeConverter::class)
abstract class DatabaseApi : RoomDatabase() {
    companion object {
        const val DATABASE_NAME = "meta_test_db"

        @JvmStatic
        fun instance(context: Context) =
                Room.databaseBuilder(context, DatabaseApi::class.java, DATABASE_NAME).build()
    }

    abstract fun metaDao(): MetaDao
    abstract fun fieldDao(): FieldDao
}