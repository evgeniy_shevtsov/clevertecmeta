package ru.test.clevertecmeta.meta.data.repository

import io.reactivex.Observable
import ru.test.clevertecmeta.meta.data.database.FieldDao
import ru.test.clevertecmeta.meta.data.database.MetaDao
import ru.test.clevertecmeta.meta.data.dto.Meta
import ru.test.clevertecmeta.meta.data.remote.MetaService


class MetaRepository(
        private val metaDao: MetaDao,
        private val fieldDao: FieldDao,
        private val metaService: MetaService
) {
    fun get(): Observable<Meta> = Observable.concatArray(getFromDb(), getFromApi())

    fun getFromApi(): Observable<Meta>
            = metaService.getMeta().doOnNext { meta ->
                meta.generateUid()
                metaDao.insert(meta)
                meta.apply {
                    fields.forEach { field ->
                        field.generateUid()
                        field.metaUid = uid
                    }
                }
                fieldDao.insert(*meta.fields.toTypedArray())
            }

    fun getFromDb(): Observable<Meta>
            = getAllFromDb().flatMap { list ->
                if (list.isEmpty())
                    Observable.empty()
                else
                    Observable.fromCallable { list.last() }
            }

    fun getAllFromDb(): Observable<List<Meta>>
            = Observable.fromCallable {
                    metaDao.getAll().apply {
                        forEach { meta -> meta.fields = fieldDao.getByMeta(meta.uid) }
                    }
                    .sortedBy { meta -> meta.createTime }
                }
}