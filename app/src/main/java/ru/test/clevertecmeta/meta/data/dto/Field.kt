package ru.test.clevertecmeta.meta.data.dto

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*


@Entity(tableName = "fields")
data class Field(

    @Expose
    @SerializedName("title")
    @ColumnInfo(name = "title")
    var title: String,

    @Expose
    @SerializedName("name")
    @ColumnInfo(name = "name")
    var name: String,

    @Expose
    @SerializedName("type")
    @ColumnInfo(name = "type")
    var type: FieldType,

    @Expose
    @SerializedName("values")
    @ColumnInfo(name = "values")
    var values: Map<String, String>? = null,

    @ColumnInfo(name = "uid")
    @PrimaryKey
    var uid: String = UUID.randomUUID().toString()
) : Serializable {

    @ColumnInfo(name = "metaUid")
    var metaUid: String = UUID.randomUUID().toString()

    @Ignore
    var value: String? = null

    fun generateUid() = run { uid = UUID.randomUUID().toString()}
}


class ValuesTypeConverter {
    @TypeConverter
    fun fromValues(values: Map<String, String>?): String =
        values?.run {
            entries.joinToString(separator = ";") { entry -> "${entry.key},${entry.value}" }
        } ?: ""

    @TypeConverter
    fun toValues(data: String): Map<String, String>? =
        if (data.isEmpty())
            null
        else
            data.split(";")
                .map {
                    item -> item.split(",").run { Pair(this[0], this[1]) }
                }
                .toTypedArray()
                .run { mapOf(*this) }
}