package ru.test.clevertecmeta.meta.presentation


import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import ru.test.clevertecmeta.R
import ru.test.clevertecmeta.base.view.BaseViewModel
import ru.test.clevertecmeta.meta.data.dto.Field
import ru.test.clevertecmeta.meta.data.dto.Meta
import ru.test.clevertecmeta.meta.data.repository.MetaRepository
import ru.test.clevertecmeta.base.extension.async
import ru.test.clevertecmeta.meta.data.dto.Result
import ru.test.clevertecmeta.meta.domain.FieldValidateUtil
import ru.test.clevertecmeta.meta.domain.SendDataUseCase
import java.util.concurrent.TimeUnit


class MetaViewModel(
    private val metaRepository: MetaRepository,
    private val sendDataUseCase: SendDataUseCase
): BaseViewModel() {

    val isLoading = MutableLiveData<Boolean?>()
    val error = MutableLiveData<Int?>()
    val meta = MutableLiveData<Meta?>()
    val result = MutableLiveData<Result?>()

    init { disposables += metaRepository.get().subscribeWithViewModel(meta) }

    fun refreshMeta() { disposables += metaRepository.getFromApi().subscribeWithViewModel(meta) }

    fun cancelLoading() {
        disposables.dispose()
        disposables = CompositeDisposable()

        isLoading.value = false
    }

    fun sendData(fields: List<Field>) {
        if (FieldValidateUtil.isValid(fields))
            disposables += sendDataUseCase.send(fields).subscribeWithViewModel(result)
        else
            error.postValue(R.string.incorrect_fields_input)
    }

    private fun <T> Observable<T>.subscribeWithViewModel(liveData: MutableLiveData<T?>)
            = this.async()
                .delay(3, TimeUnit.SECONDS)
                .doOnSubscribe { isLoading.postValue(true) }
                .doOnError { error.postValue(R.string.meta_load_error) }
                .subscribe ({ loaded ->
                    liveData.postValue(loaded)
                    isLoading.postValue(false)
                    error.postValue(null)
                }, { throwable ->
                    throwable.printStackTrace()
                    error.postValue(R.string.meta_load_error)
                })
}