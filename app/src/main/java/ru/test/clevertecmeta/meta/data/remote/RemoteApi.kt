package ru.test.clevertecmeta.meta.data.remote

import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class RemoteApi(
    gson: Gson,
    okHttpClient: OkHttpClient
) {
    private val retrofit: Retrofit

    companion object {
        const val BASE_URL = "http://test.clevertec.ru/tt/"
    }

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    fun metaSetvice(): MetaService = retrofit.create(MetaService::class.java)
    fun dataSetvice(): FormDataService = retrofit.create(FormDataService::class.java)
}