package ru.test.clevertecmeta.meta.data.remote

import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST
import ru.test.clevertecmeta.meta.data.dto.FormData
import ru.test.clevertecmeta.meta.data.dto.Result


interface FormDataService {

    @POST("data")
    fun sendData(@Body formData: FormData): Observable<Result>
}