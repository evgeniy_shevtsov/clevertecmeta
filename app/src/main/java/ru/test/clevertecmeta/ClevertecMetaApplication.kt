package ru.test.clevertecmeta

import dagger.android.AndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.DaggerApplication
import ru.test.clevertecmeta.di.DaggerAppComponent


class ClevertecMetaApplication : DaggerApplication(), HasActivityInjector {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication>
       = DaggerAppComponent.builder().create(this)
}