package ru.test.clevertecmeta.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.test.clevertecmeta.meta.MetaActivity
import ru.test.clevertecmeta.meta.presentation.MetaFragment


@Module
abstract class BuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeMetaActivity(): MetaActivity

    @ContributesAndroidInjector
    abstract fun contributeMetaFragment(): MetaFragment
}