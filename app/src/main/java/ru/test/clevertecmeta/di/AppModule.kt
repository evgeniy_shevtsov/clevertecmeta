package ru.test.clevertecmeta.di

import dagger.Module
import javax.inject.Singleton
import dagger.Provides
import ru.test.clevertecmeta.ClevertecMetaApplication


@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: ClevertecMetaApplication) = application.getApplicationContext()
}