package ru.test.clevertecmeta.di

import dagger.Module
import dagger.Provides
import ru.test.clevertecmeta.meta.data.repository.FormDataRepository
import ru.test.clevertecmeta.meta.data.repository.MetaRepository
import ru.test.clevertecmeta.meta.presentation.MetaViewModelFactory
import ru.test.clevertecmeta.meta.domain.SendDataUseCase
import javax.inject.Singleton


@Module
class DomainModule {

    @Provides
    @Singleton
    fun provideSendDataUseCase(dataRepository: FormDataRepository) = SendDataUseCase(dataRepository)

    @Provides
    @Singleton
    fun provideMetaViewModelFactory(metaRepository: MetaRepository, sendDataUseCase: SendDataUseCase)
            = MetaViewModelFactory(metaRepository, sendDataUseCase)
}