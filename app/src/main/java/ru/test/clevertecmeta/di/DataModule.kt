package ru.test.clevertecmeta.di

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.test.clevertecmeta.meta.data.database.DatabaseApi
import ru.test.clevertecmeta.meta.data.remote.RemoteApi
import ru.test.clevertecmeta.meta.data.repository.FormDataRepository
import ru.test.clevertecmeta.meta.data.repository.MetaRepository
import javax.inject.Singleton


@Module
class DataModule {

    @Provides
    @Singleton
    fun provideDatabaseApi(context: Context) = DatabaseApi.instance(context)

    @Provides
    @Singleton
    fun provideMetaRepository(db: DatabaseApi, remote: RemoteApi)
            = MetaRepository(db.metaDao(), db.fieldDao(), remote.metaSetvice())

    @Provides
    @Singleton
    fun provideDataRepository(remote: RemoteApi) = FormDataRepository(remote.dataSetvice())
}