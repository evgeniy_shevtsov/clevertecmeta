package ru.test.clevertecmeta.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import ru.test.clevertecmeta.meta.data.remote.FormDataService
import ru.test.clevertecmeta.meta.data.remote.MetaService
import ru.test.clevertecmeta.meta.data.remote.RemoteApi
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideGson(): Gson 
            = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .excludeFieldsWithoutExposeAnnotation()
                    .create()

    @Provides
    @Singleton
    fun provideOkHttpClient()
            = OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC })
                    .build()

    @Provides
    @Singleton
    fun provideRemoteApi(gson: Gson, okHttpClient: OkHttpClient) = RemoteApi(gson, okHttpClient)

    @Provides
    @Singleton
    fun provideMetaService(remoteApi: RemoteApi): MetaService = remoteApi.metaSetvice()

    @Provides
    @Singleton
    fun provideDataService(remoteApi: RemoteApi): FormDataService = remoteApi.dataSetvice()
}