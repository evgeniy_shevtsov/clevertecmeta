package ru.test.clevertecmeta.di

import dagger.Component
import ru.test.clevertecmeta.ClevertecMetaApplication
import javax.inject.Singleton
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class,
        BuilderModule::class,
        NetworkModule::class,
        AppModule::class,
        DataModule::class,
        DomainModule::class]
)
interface AppComponent : AndroidInjector<ClevertecMetaApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<ClevertecMetaApplication>()
}