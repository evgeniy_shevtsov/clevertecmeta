package ru.test.clevertecmeta.base

import android.text.Editable
import android.text.TextWatcher


class TextChangedListener(
    private val onTextChanged: (String) -> Unit
) : TextWatcher {

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(s: Editable?) = onTextChanged(s.toString())
}