package ru.test.clevertecmeta.base

import android.view.View
import android.widget.AdapterView


class OnItemSelectedListener(
    private val onItemSelected: (position: Int) -> Unit
) : AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) { }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) = onItemSelected(position)
}