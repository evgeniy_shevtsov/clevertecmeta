package ru.test.clevertecmeta.base

typealias OnClickListener<T> = (item: T) -> Unit
typealias OnLongClickListener<T> = (item: T) -> Boolean