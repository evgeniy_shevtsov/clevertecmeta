package ru.test.clevertecmeta.base.view

import android.os.Bundle
import androidx.navigation.findNavController
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import ru.test.clevertecmeta.base.Router
import ru.test.clevertecmeta.base.extension.hideKeyboard


abstract class BaseFragment: DaggerFragment(), Router {
    override fun router() = view!!.findNavController()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onPause() {
        activity?.hideKeyboard()
        super.onPause()
    }
}