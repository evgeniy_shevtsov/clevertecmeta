package ru.test.clevertecmeta.base

import androidx.navigation.NavController

interface Router {
    fun router(): NavController
}