package ru.test.clevertecmeta.base.view

import android.os.Bundle
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity
import ru.test.clevertecmeta.base.Router


abstract class BaseActivity : DaggerAppCompatActivity(), Router {
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }
}