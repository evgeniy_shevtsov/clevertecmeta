package ru.test.clevertecmeta.base.view

import android.os.Bundle
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerAppCompatDialogFragment
import ru.test.clevertecmeta.base.Router

abstract class BaseDialogFragment : DaggerAppCompatDialogFragment(), Router {
    override fun router() =
            if (activity is BaseActivity)
                (activity as BaseActivity).router()
            else
                throw IllegalStateException(
                        "Can't route BaseFragment in ${activity?.javaClass?.simpleName}." +
                                " Use only with BaseActivity instance."
                )

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }
}