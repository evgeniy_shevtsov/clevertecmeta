package ru.test.clevertecmeta.base.extension

import android.app.Activity
import android.view.inputmethod.InputMethodManager


fun Activity.hideKeyboard() {
    currentFocus?.let { focused ->
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(focused.windowToken, 0)
    }
}